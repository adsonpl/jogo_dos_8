object Main {

  var open, closed: List[Node] = List()
  var expandedCount: Int = 0

  def main(args: Array[String]) = {
//  addToOpen(Node.generaTeNodeFromList(List(2,1,3,5,9,7,8,4,6)))
    addToOpen(Node.newInstance)
    calculate
  }

  def calculate: Unit = {
    val node = getBestNode
    if (node.isObjective) {
      node.printTree
      println("Expanded count: " + expandedCount)
    } else {
      node.printTable
      node.getChildren.foreach(addToOpen)
      calculate
    }
  }

  def verifyNode(node: Node) = !closed.contains(node) && !open.contains(node)

  def getBestNode = {
    val node :: newOpen = open.sortWith(_.value < _.value)
    open = newOpen
    addToClosed(node)
    node
  }

  def addToOpen(node: Node) = {
    if (verifyNode(node)){
      expandedCount += 1
      open = open :+ node
    }
  }

  def addToClosed(node: Node) = closed = closed :+ node

}
