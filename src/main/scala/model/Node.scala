import scala.util.Random

import scalaz._
import Scalaz._

object Node {

  val EmptyValue = 9

  val Indices = List.fill(2)((0 to 2) toList).sequence

  val OrderedList = (1 to 9).toList

  val Objective = generateTableFromList(OrderedList)

  val InvertedTable = Objective.map(_.swap)

  val MovesHash = Indices.map(getMovesTuple).toMap

  var maxValue: Int = _

  def getMoves(pos: List[Int]) = posToList(pos).map(_.sequence).flatten

  def posToList(pos: List[Int]) = pos.zipWithIndex.map(zippedValues(pos, _))

  def zippedValues(pos: List[Int], t: (Int, Int)) = pos.updated(t._2, getPossibleMove(t._1)).map(convertToList)

  def convertToList(any: Any): List[Int] = (if (any.isInstanceOf[Int]) List(any) else any).asInstanceOf[List[Int]]

  def getMovesTuple(index: List[Int]) = (index, getMoves(index))

  def newInstance = generaTeNodeFromList(shuffleList)

  def generaTeNodeFromList(list: List[Int]) = {
    val node = new Node(generateTableFromList(list))
    maxValue = node.value
    node
  }

  def generateTableFromList(list: List[Int]) = {
    list.zipWithIndex map { case (e, i) =>
      List(i / 3, i % 3) -> e
    } toMap
  }

  def shuffleList:List[Int] = {
    val shuffled = Random.shuffle(OrderedList)
    val toTest = shuffled diff List(EmptyValue)
    val inverted: Int = toTest.zipWithIndex.foldLeft(0) { case (acc, (value, index)) =>
      acc + toTest.slice(index + 1, toTest.length).count(test => value > test)
    }
    if (inverted % 2 == 0)
      shuffled
    else
      shuffleList
  }

  def getPossibleMove(value: Int) = value match {
    case 0 | 2 => List(1)
    case 1 => List(0, 2)
  }

}

class Node(var table: Map[List[Int], Int], val parent: Option[Node] = None, var cost: Int = 0) {

  def getEmptyPos = table.find(_._2 == Node.EmptyValue).get._1

  val value = cost + (misplacedTiles + manhattanDistance) / 2

  def misplacedTiles = {
    table.count { case (k, v) =>
      v != Node.EmptyValue && v != Node.Objective(k)
    }
  }

  def manhattanDistance = {
    table.map { case (k, v) =>
      k.zip(Node.InvertedTable(v)).map(t => (t._1 - t._2).abs).sum
    }.sum
  }

  override def equals(o: Any) = o match {
    case that: Node => this.table == that.table
    case _ => false
  }

  def isObjective = this.table == Node.Objective

  def printTable {
    Node.Indices.grouped(3).foreach { e =>
      println(e.map(table(_)) mkString ",")
    }
    println("---")
  }

  def generateChild(actual: List[Int], newMove: List[Int]) = new Node(table.updated(actual, table(newMove)).updated(newMove, table(actual)), Some(this), cost + 1)

  def getChildren = {
    val actual = getEmptyPos
    Node.MovesHash(actual).map(generateChild(actual, _))
  }

  def getTree(list: List[Node] = List()): List[Node] = {
    val newList = list :+ this
    parent match {
      case Some(p) =>
        p.getTree(newList)
      case None =>
        newList.reverse
    }
  }

  def printTree = {
    val nodes = getTree()
    println("Result tree")
    println("======")
    nodes.foreach(_.printTable)
    println("======")
    println("number of nodes: " + nodes.length)
  }

}
