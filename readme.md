

generateChild - gera um novo nodo a partir da posição do espaço e de uma nova posição

getChildren - gera todos os filhos possíveis

Métodos principais da classe Main:

calculate - método recursivo que fica em loop até encontrar o nodo objetivo

getBestNode - método que busca o nodo com melhor avaliação da lista de nodos abertos (fronteira)

Refereências:
https://heuristicswiki.wikispaces.com/N+-+Puzzle